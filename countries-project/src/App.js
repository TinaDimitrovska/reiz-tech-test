import "./App.css";
import ListCountries from "./Components/ListCountries";
import "bootstrap/dist/css/bootstrap.min.css";

function App() {
  return (
    <div className="App">
      <ListCountries />
    </div>
  );
}

export default App;
