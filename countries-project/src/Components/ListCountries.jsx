import React, { useEffect, useState } from "react";
import { Button, ListGroupItem, Pagination } from "react-bootstrap";

export default function ListCountries() {
  const [data, setData] = useState([]);
  const [sortType, setSortType] = useState("asc");
  const [resetData, setResetData] = useState([]);
  const [currPage, setCurrPage] = useState(1);
  const [countriesByPage] = useState(10);

  useEffect(() => {
    const apiUrl = `https://restcountries.com/v2/all?fields=name,region,area`;
    fetch(apiUrl)
      .then((response) => response.json())
      .then((data) => {
        setData(data);
        setResetData(data);
      });
  }, []);

  console.log(data);

  //Sort by Ascending/Descending

  const sorted = data.sort((a, b) => {
    const isReversed = sortType === "asc" ? 1 : -1;

    return isReversed * a.name.localeCompare(b.name);
  });

  const onSort = (sortType) => {
    setSortType(sortType);
  };

  //Sort the countries by area that are smaller then Lithuania

  const areaSort = () => {
    let response = data.filter((country) => {
      if (country.area < data[127].area) {
        return country;
      }
    });
    setData(response);
  };

  //Sort the countries by region Oceania

  const sortByOceania = () => {
    let oceaniaRegion = data.filter((oceania) => {
      if (oceania.region === "Oceania") {
        return oceania;
      }
    });
    setData(oceaniaRegion);
  };

  //Reset the filters

  const resetFilters = () => {
    setData(resetData);
  };

  //Get the current Countries

  const indexOfLastCountry = currPage * countriesByPage;
  const indexOfFirstCountry = indexOfLastCountry - countriesByPage;
  const currentCountries = data.slice(indexOfFirstCountry, indexOfLastCountry);

  //On click change the page

  const pagination = (pageNumber) => {
    setCurrPage(pageNumber);
  };

  //Create the Pagination items

  let items = [];
  for (let i = 1; i <= Math.ceil(data.length / countriesByPage); i++) {
    items.push(
      <Pagination.Item key={i} onClick={() => pagination(i)}>
        {i}
      </Pagination.Item>
    );
  }

  return (
    <>
      <div className="container">
        <h1 className="header">List of countries</h1>

        <Button className="left" onClick={() => onSort("asc")}>
          Sort by Ascending
        </Button>
        <Button className="left margin" onClick={() => onSort("desc")}>
          Sort by Descending
        </Button>
        <Button className="filter" onClick={resetFilters}>
          Reset Filters
        </Button>
        <Button className="filter" onClick={areaSort}>
          Sort by area
        </Button>
        <Button className="filter" onClick={sortByOceania}>
          Sort by Oceania Region
        </Button>
        <div className="list">
          {currentCountries.map((country) => {
            return (
              <ListGroupItem className="green" key={country.name}>
                <p className="details">Country: {country.name}</p>
                <p className="details">Region: {country.region}</p>
                <p className="details">Area: {country.area}</p>
              </ListGroupItem>
            );
          })}
        </div>
        <div className="paginate">
          {" "}
          <Pagination>{items}</Pagination>
          <br />
        </div>
      </div>
    </>
  );
}
